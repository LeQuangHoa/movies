package com.kapsulo.apps.movies

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

object LiveDataTestUtil {
	
	@Throws(InterruptedException::class)
	fun <T> getValue(liveData: LiveData<T>): T {
		val data = arrayOfNulls<Any>(1)
		val latch = CountDownLatch(1)
		val observer = object : Observer<T> {
			override fun onChanged(type: T?) {
				data[0] = type
				latch.countDown()
				liveData.removeObserver(this)
			}
		}
		liveData.observeForever(observer)
		latch.await(2, TimeUnit.SECONDS)
		@Suppress("UNCHECKED_CAST")
		return data[0] as T
	}
}