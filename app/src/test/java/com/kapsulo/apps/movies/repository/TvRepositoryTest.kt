package com.kapsulo.apps.movies.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.kapsulo.apps.movies.MockTestUtil
import com.kapsulo.apps.movies.api.ApiUtil
import com.kapsulo.apps.movies.api.TvService
import com.kapsulo.apps.movies.models.Keyword
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Review
import com.kapsulo.apps.movies.models.Video
import com.kapsulo.apps.movies.models.networks.KeywordListResponse
import com.kapsulo.apps.movies.models.networks.ReviewListResponse
import com.kapsulo.apps.movies.models.networks.VideoListResponse
import com.kapsulo.apps.movies.room.TvDao
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class TvRepositoryTest {
	
	private lateinit var repository: TvRepository
	private val tvDao = mock<TvDao>()
	private val service = mock<TvService>()
	
	@Rule
	@JvmField
	val instantExecutorRule = InstantTaskExecutorRule()
	
	@Before
	fun init() {
		repository = TvRepository(service, tvDao)
	}
	
	@Test
	fun loadKeywordListFromNetwork() {
		val loadFromDB = MockTestUtil.mockTv()
		whenever(tvDao.getTv(123)).thenReturn(loadFromDB)
		
		// Assume a response from the api service by mocking a list of keywords.
		val mockResponse = KeywordListResponse(123, MockTestUtil.mockKeywordList())
		
		// Asserts an successful call from service.
		val call = ApiUtil.successCall(mockResponse)
		
		// Check value from the fetch-keywords method.
		whenever(service.fetchKeywords(123)).thenReturn(call)

		val data = repository.loadKeywordList(123)
		verify(tvDao).getTv(123)
		verifyNoMoreInteractions(service)
		
		val observer = mock<Observer<Resource<List<Keyword>>>>()
		data.observeForever(observer)
		verify(observer).onChanged(Resource.success(MockTestUtil.mockKeywordList(), true))
		
		val updatedTv = MockTestUtil.mockTv()
		updatedTv.keywords = MockTestUtil.mockKeywordList()
		verify(tvDao).update(updatedTv)
	}
	
	@Test
	fun loadVideoListFromNetwork() {
		val loadFromDB = MockTestUtil.mockTv()
		whenever(tvDao.getTv(123)).thenReturn(loadFromDB)
		
		val mockResponse = VideoListResponse(123, MockTestUtil.mockVideoList())
		val call = ApiUtil.successCall(mockResponse)
		whenever(service.fetchVideos(123)).thenReturn(call)
		
		val data = repository.loadVideoList(123)
		verify(tvDao).getTv(123)
		verifyNoMoreInteractions(service)
		
		val observer = mock<Observer<Resource<List<Video>>>>()
		data.observeForever(observer)
		verify(observer).onChanged(Resource.success(MockTestUtil.mockVideoList(), false))
		
		val updatedTv = MockTestUtil.mockTv()
		updatedTv.videos = MockTestUtil.mockVideoList()
		verify(tvDao).update(updatedTv)
	}

	@Test
	fun loadReviewListFromNetwork() {
		val loadFromDB = MockTestUtil.mockTv()
		whenever(tvDao.getTv(123)).thenReturn(loadFromDB)
		
		val mockResponse = ReviewListResponse(123, 1, MockTestUtil.mockReviewList(), 100, 100)
		val call = ApiUtil.successCall(mockResponse)
		whenever(service.fetchReviews(123)).thenReturn(call)
		
		val data = repository.loadReviewsList(123)
		verify(tvDao).getTv(123)
		verifyNoMoreInteractions(service)
		
		val observer = mock<Observer<Resource<List<Review>>>>()
		data.observeForever(observer)
		verify(observer).onChanged(Resource.success(MockTestUtil.mockReviewList(), false))
		
		val updatedTv = MockTestUtil.mockTv()
		updatedTv.reviews = MockTestUtil.mockReviewList()
		verify(tvDao).update(updatedTv)
	}
}