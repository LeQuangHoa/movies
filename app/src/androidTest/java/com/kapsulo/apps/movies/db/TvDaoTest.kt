package com.kapsulo.apps.movies.db

import android.support.test.runner.AndroidJUnit4
import com.kapsulo.apps.movies.LiveDataTestUtil
import com.kapsulo.apps.movies.MockTestUtil
import com.kapsulo.apps.movies.models.entity.Tv
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TvDaoTest: DBTest() {
	
	@Test
	fun insertAndRead() {
		val tvList = ArrayList<Tv>()
		val tv = MockTestUtil.mockTv()
		tvList.add(tv)
		
		db.tvDao().insert(tvList)
		val loadedFromDB = LiveDataTestUtil.getValue(db.tvDao().getTvList(tv.page))[0]
		MatcherAssert.assertThat(loadedFromDB.page, CoreMatchers.`is`(1))
		MatcherAssert.assertThat(loadedFromDB.id, CoreMatchers.`is`(123))
	}
	
	@Test
	fun updateAndReadTest() {
		val tvList = ArrayList<Tv>()
		val tv = MockTestUtil.mockTv()
		tvList.add(tv)
		db.tvDao().insert(tvList)
		
		val loadedFromDB = db.tvDao().getTv(tv.id)
		MatcherAssert.assertThat(loadedFromDB.page, CoreMatchers.`is`(1))
		
		tv.page = 10
		db.tvDao().update(tv)
		
		val updated = db.tvDao().getTv(tv.id)
		MatcherAssert.assertThat(updated.page, CoreMatchers.`is`(10))
	}
}