package com.kapsulo.apps.movies.view.ui.main

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kapsulo.apps.movies.R
import com.kapsulo.apps.movies.extension.observeLiveData
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Status
import com.kapsulo.apps.movies.models.entity.Tv
import com.kapsulo.apps.movies.view.adapter.TvListAdapter
import com.kapsulo.apps.movies.view.viewholder.TvListViewHolder
import com.kapsulo.apps.movies.viewmodel.MainActivityViewModel
import com.skydoves.baserecyclerviewadapter.RecyclerViewPaginator
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_main_tv.*
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

class TvListFragment: Fragment(), TvListViewHolder.Delegate {
	
	@Inject
	lateinit var viewModelFactory: ViewModelProvider.Factory
	private lateinit var viewModel: MainActivityViewModel

	private val adapter = TvListAdapter(this)
	private lateinit var paginator: RecyclerViewPaginator

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_main_tv, container, false)
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		initializeUI()
	}
	
	override fun onAttach(context: Context?) {
		AndroidSupportInjection.inject(this)
		super.onAttach(context)
		
		viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel::class.java)
		observeViewModel()
	}

	private fun initializeUI() {
		recycler_view.adapter = adapter
		recycler_view.layoutManager = GridLayoutManager(context, 2)
		paginator = RecyclerViewPaginator(recyclerView = recycler_view,
				isLoading = { viewModel.getTvListValues()?.status == Status.LOADING},
				loadMore = { loadMore(it) },
				onLast = { viewModel.getTvListValues()?.onLastPage!! })
		paginator.currentPage = 1
	}

	private fun loadMore(page: Int) {
		viewModel.postTvPage(page)
	}

	private fun observeViewModel() {
		observeLiveData(viewModel.getTvListObservable()) { updateTvList(it) }
		viewModel.postTvPage(1)
	}
	
	private fun updateTvList(resource: Resource<List<Tv>>) {
		when (resource.status) {
			Status.SUCCESS -> { adapter.addTvList(resource)}
			Status.ERROR -> toast(resource.errorModel?.statusMessage.toString())
			Status.LOADING -> {}
		}
	}

	override fun onItemClick(tv: Tv) {}
}