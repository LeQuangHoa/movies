package com.kapsulo.apps.movies.view.viewholder

import android.view.View
import com.bumptech.glide.Glide
import com.github.florent37.glidepalette.BitmapPalette
import com.github.florent37.glidepalette.GlidePalette
import com.kapsulo.apps.movies.api.Api
import com.kapsulo.apps.movies.models.entity.Tv
import com.skydoves.baserecyclerviewadapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_poster.view.*

class TvListViewHolder(val view: View, val delegate: Delegate) : BaseViewHolder(view) {

	interface Delegate {
		fun onItemClick(tv: Tv)
	}

	private lateinit var tv: Tv

	override fun bindData(data: Any) {
		if (data is Tv) {
			tv = data
			drawItem()
		}
	}

	private fun drawItem() {
		itemView.run {
			item_poster_tv_title.text = tv.name
			tv.posterPath?.let {
				Glide.with(context).load(Api.getPosterPath(it))
						.listener(GlidePalette.with(Api.getPosterPath(it))
								.use(BitmapPalette.Profile.VIBRANT)
								.intoBackground(item_poster_ll_palette)
								.crossfade(true))
						.into(item_poster_iv_post)
			}
		}
	}

	override fun onClick(v: View?) {
		delegate.onItemClick(tv)
	}

	override fun onLongClick(v: View?): Boolean = false
}