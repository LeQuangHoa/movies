package com.kapsulo.apps.movies.view.ui.main

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kapsulo.apps.movies.R
import com.kapsulo.apps.movies.extension.observeLiveData
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.Status
import com.kapsulo.apps.movies.models.entity.Person
import com.kapsulo.apps.movies.view.adapter.PeopleAdapter
import com.kapsulo.apps.movies.view.viewholder.PeopleViewHolder
import com.kapsulo.apps.movies.viewmodel.MainActivityViewModel
import com.skydoves.baserecyclerviewadapter.RecyclerViewPaginator
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_main_tv.*
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

class PersonListFragment : Fragment(), PeopleViewHolder.Delegate {

	@Inject
	lateinit var viewModelFactory: ViewModelProvider.Factory
	private lateinit var viewModel: MainActivityViewModel

	private val adapter = PeopleAdapter(this)
	private lateinit var paginator: RecyclerViewPaginator

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_main_star, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		initializeUI()
	}

	override fun onAttach(context: Context?) {
		AndroidSupportInjection.inject(this)
		super.onAttach(context)

		viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel::class.java)
		observeViewModel()
	}

	private fun initializeUI() {
		recycler_view.adapter = adapter
		recycler_view.layoutManager = GridLayoutManager(context, 2)
		paginator = RecyclerViewPaginator(recyclerView = recycler_view,
				isLoading = { viewModel.getPeopleValues()?.status == Status.LOADING },
				loadMore = { loadMore(it) },
				onLast = { viewModel.getPeopleValues()?.onLastPage!! })
		paginator.currentPage = 1
	}

	private fun observeViewModel() {
		observeLiveData(viewModel.getPeopleObservable()) { updatePeople(it) }
		viewModel.postPeoplePage(1)
	}

	private fun updatePeople(resource: Resource<List<Person>>) {
		when (resource.status) {
			Status.SUCCESS -> adapter.addPeople(resource)
			Status.ERROR -> toast(resource.errorModel?.statusMessage.toString())
			Status.LOADING -> {}
		}
	}

	private fun loadMore(page: Int) {
		viewModel.postPeoplePage(page)
	}

	override fun onItemClick(person: Person, view: View) {}
}