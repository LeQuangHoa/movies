package com.kapsulo.apps.movies.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.kapsulo.apps.movies.viewmodel.AppViewModelFactory
import com.kapsulo.apps.movies.viewmodel.MainActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
internal abstract class ViewModelModule {
	
	@Binds
	@IntoMap
	@ViewModelKey(MainActivityViewModel::class)
	internal abstract fun bindMainActivityViewModels(mainActivityViewModel: MainActivityViewModel): ViewModel
	
	@Binds
	internal abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory
}