package com.kapsulo.apps.movies.view.viewholder

import android.view.View
import com.bumptech.glide.Glide
import com.github.florent37.glidepalette.BitmapPalette
import com.github.florent37.glidepalette.GlidePalette
import com.kapsulo.apps.movies.api.Api
import com.kapsulo.apps.movies.models.entity.Movie
import com.skydoves.baserecyclerviewadapter.BaseViewHolder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_poster.view.*

class MovieListViewHolder(val view: View, private val delegate: Delegate) : BaseViewHolder(view) {

	interface Delegate {
		fun onItemClick(movie: Movie)
	}

	private lateinit var movie: Movie

	override fun bindData(data: Any) {
		if (data is Movie) {
			movie = data
			drawItem()
		}
	}

	private fun drawItem() {
		itemView.run {
			item_poster_tv_title.text = movie.title
			movie.posterPath?.let {
				Glide.with(context)
						.load(Api.getPosterPath(it))
						.listener(GlidePalette.with(Api.getPosterPath(it))
								.use(BitmapPalette.Profile.VIBRANT)
								.intoBackground(item_poster_ll_palette)
								.crossfade(true))
						.into(item_poster_iv_post)
			}
		}
	}

	override fun onClick(v: View?) {
		delegate.onItemClick(movie)
	}

	override fun onLongClick(v: View?): Boolean = false
}