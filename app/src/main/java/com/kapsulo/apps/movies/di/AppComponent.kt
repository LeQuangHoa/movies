package com.kapsulo.apps.movies.di

import android.app.Application
import android.os.PersistableBundle
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
	AndroidInjectionModule::class,
	AndroidSupportInjectionModule::class,
	ActivityModule::class,
	ViewModelModule::class,
	NetworkModule::class,
	PersistenceModule::class
])
interface AppComponent: AndroidInjector<DaggerApplication> {
	
	@Component.Builder
	interface Builder {
		@BindsInstance
		fun application(application: Application): AppComponent.Builder
		fun build(): AppComponent
	}
	
	override fun inject(instance: DaggerApplication)
}