package com.kapsulo.apps.movies.models.networks

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.kapsulo.apps.movies.models.NetworkResponseModel

data class PersonDetail(
		@SerializedName("birthday")
		val birthDay: String?,
		
		@SerializedName("known_for_department")
		val knownForDepartment: String?,
		
		@SerializedName("place_of_birth")
		val placeOfBirth: String?,
		
		@SerializedName("also_known_as")
		val alsoKnownAs: List<String>?,
		
		@SerializedName("biography")
		val biography: String?
) : Parcelable, NetworkResponseModel {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.createStringArrayList(),
			parcel.readString())
	
	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(birthDay)
		parcel.writeString(knownForDepartment)
		parcel.writeString(placeOfBirth)
		parcel.writeStringList(alsoKnownAs)
		parcel.writeString(biography)
	}
	
	override fun describeContents(): Int {
		return 0
	}
	
	companion object CREATOR : Parcelable.Creator<PersonDetail> {
		override fun createFromParcel(parcel: Parcel): PersonDetail {
			return PersonDetail(parcel)
		}
		
		override fun newArray(size: Int): Array<PersonDetail?> {
			return arrayOfNulls(size)
		}
	}
}