package com.kapsulo.apps.movies.di

import com.kapsulo.apps.movies.view.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
	
	@ContributesAndroidInjector(modules = [MainActivityFragmentModule::class])
	abstract fun contributeMainActivity(): MainActivity
}