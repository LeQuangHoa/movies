package com.kapsulo.apps.movies.models.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.kapsulo.apps.movies.models.networks.PersonDetail

@Entity(tableName = "People", primaryKeys = ["id"])
data class Person(
		var page: Int,
		
		@Embedded
		var personDetail: PersonDetail? = null,
		
		@SerializedName("profile_path")
		val profilePath: String?,
		
		@SerializedName("adult")
		val adult: Boolean,
		
		@SerializedName("id")
		val id: Int,
		
		@SerializedName("name")
		val name: String,
		
		@SerializedName("popularity")
		val popularity: Float) : Parcelable {
	
	constructor(parcel: Parcel) : this(
			parcel.readInt(),
			parcel.readParcelable<PersonDetail>(PersonDetail::class.java.classLoader),
			parcel.readString(),
			1 == parcel.readInt(),
			parcel.readInt(),
			parcel.readString()!!,
			parcel.readFloat()) {
	}
	
	override fun writeToParcel(dest: Parcel?, flags: Int) = with(dest!!) {
		writeInt(page)
		writeParcelable(personDetail, 0)
		writeString(profilePath)
		writeInt(if (adult) 1 else 0)
		writeInt(id)
		writeString(name)
		writeFloat(popularity)
	}
	
	override fun describeContents(): Int = 0
	
	companion object CREATOR : Parcelable.Creator<Person> {
		override fun createFromParcel(parcel: Parcel): Person {
			return Person(parcel)
		}
		
		override fun newArray(size: Int): Array<Person?> {
			return arrayOfNulls(size)
		}
	}
}