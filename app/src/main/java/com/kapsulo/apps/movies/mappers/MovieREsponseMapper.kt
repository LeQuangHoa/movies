package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.networks.DiscoverMovieResponse

class MovieREsponseMapper : NetworkResponseMapper<DiscoverMovieResponse> {
	override fun onLastPage(response: DiscoverMovieResponse): Boolean {
		return true
	}
}
