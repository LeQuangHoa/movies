package com.kapsulo.apps.movies

import com.facebook.stetho.Stetho
import com.kapsulo.apps.movies.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class TheMoviesApp: DaggerApplication() {
	
	private val appComponent = DaggerAppComponent.builder()
			.application(this)
			.build()
	
	override fun onCreate() {
		super.onCreate()
		
		if (BuildConfig.DEBUG) {
			Timber.plant(Timber.DebugTree())
		}
		Stetho.initializeWithDefaults(this)
	}
	
	override fun applicationInjector(): AndroidInjector<out DaggerApplication> = appComponent
}