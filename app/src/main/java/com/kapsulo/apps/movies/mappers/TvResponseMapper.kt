package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.networks.DiscoverTvResponse

class TvResponseMapper() : NetworkResponseMapper<DiscoverTvResponse> {
	override fun onLastPage(response: DiscoverTvResponse): Boolean {
		return true
	}
}