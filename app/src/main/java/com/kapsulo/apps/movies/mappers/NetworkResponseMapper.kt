package com.kapsulo.apps.movies.mappers

import com.kapsulo.apps.movies.models.NetworkResponseModel

interface NetworkResponseMapper<in From : NetworkResponseModel> {
	fun onLastPage(response: From): Boolean
}