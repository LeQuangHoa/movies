package com.kapsulo.apps.movies.repository

import android.arch.lifecycle.LiveData
import com.kapsulo.apps.movies.api.ApiResponse
import com.kapsulo.apps.movies.api.TheDiscoverService
import com.kapsulo.apps.movies.mappers.MovieREsponseMapper
import com.kapsulo.apps.movies.mappers.TvResponseMapper
import com.kapsulo.apps.movies.models.Resource
import com.kapsulo.apps.movies.models.entity.Movie
import com.kapsulo.apps.movies.models.entity.Tv
import com.kapsulo.apps.movies.models.networks.DiscoverMovieResponse
import com.kapsulo.apps.movies.models.networks.DiscoverTvResponse
import com.kapsulo.apps.movies.room.MovieDao
import com.kapsulo.apps.movies.room.TvDao
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DiscoverRepository @Inject
constructor(val service: TheDiscoverService, val movieDao: MovieDao, val tvDao: TvDao): Repository {
	
	fun loadMovies(pages: Int): LiveData<Resource<List<Movie>>> {
		return object : NetworkBoundRepository<List<Movie>, DiscoverMovieResponse, MovieREsponseMapper>() {
			override fun saveFetchData(items: DiscoverMovieResponse) {
				for (item in items.results) {
					item.page = pages
				}
				movieDao.insertMovieList(movies = items.results)
			}
			
			override fun shouldFetch(data: List<Movie>?): Boolean {
				return data == null || data.isEmpty()
			}
			
			override fun loadFromDB(): LiveData<List<Movie>> {
				return movieDao.getMovieList(page_ = pages)
			}
			
			override fun fetchService(): LiveData<ApiResponse<DiscoverMovieResponse>> {
				return service.fetchDiscoverMovie(page = pages)
			}
			
			override fun mapper(): MovieREsponseMapper {
				return MovieREsponseMapper()
			}
			
			override fun onFetchFailed(message: String?) {
				Timber.d("onFetchFailed: $message")
			}
		}.asLiveData()
	}
	
	fun loadTvs(pages: Int): LiveData<Resource<List<Tv>>> {
		return object : NetworkBoundRepository<List<Tv>, DiscoverTvResponse, TvResponseMapper>() {
			override fun saveFetchData(items: DiscoverTvResponse) {
				for (item in items.results) {
					item.page = pages
				}
				tvDao.insert(tvs = items.results)
			}
			
			override fun shouldFetch(data: List<Tv>?): Boolean {
				return data == null || data.isEmpty()
			}
			
			override fun loadFromDB(): LiveData<List<Tv>> {
				return tvDao.getTvList(page_ = pages)
			}
			
			override fun fetchService(): LiveData<ApiResponse<DiscoverTvResponse>> {
				return service.fetchDiscoverTv(page = pages)
			}
			
			override fun mapper(): TvResponseMapper {
				return TvResponseMapper()
			}
			
			override fun onFetchFailed(message: String?) {
				Timber.d("onFetchFailed: $message")
			}
			
		}.asLiveData()
	}
}
